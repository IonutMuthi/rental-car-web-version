﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication9.Controllers;

namespace WebApplication9.Controllers
{
    public class aboutController : Controller
    {
        
        // GET: about
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public ActionResult AjaxMethod(string name)
        {
            int id = int.Parse(name);

           return RedirectToAction("~/Customer/Edit",id);
        }
    }
}