﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication9.Models;

namespace WebApplication9.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {  
            using(RentalCarEntities db = new RentalCarEntities())
            {
                return View(db.Customers.ToList());
            }
           
        }

        public ActionResult EditList()
        {
            using (RentalCarEntities db = new RentalCarEntities())
            {
                return View(db.Customers.ToList());
            }

        }

       

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            try
            {
                using(RentalCarEntities db = new RentalCarEntities())
                {
                    db.Customers.Add(customer);
                    db.SaveChanges();
                }

                return Redirect("~/Home/Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            using (RentalCarEntities db =new RentalCarEntities())
            {
                return View(db.Customers.Where(x=>x.CostumerID==id).FirstOrDefault());
            }
            
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id ,Customer customer)
        {
            try
            {
                using(RentalCarEntities db = new RentalCarEntities())
                {
                    db.Entry(customer).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return Redirect("~/Home/Index");
            }
            catch
            {
                return View();
            }
        }

    }
}
