﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication9.Models;

namespace WebApplication9.Controllers
{
    public class RentController : Controller
    {
        // GET: Rent
        public ActionResult Index()
        {
            using(RentalCarEntities db = new RentalCarEntities())
            {
                return View(db.Reservations.ToList());
            }
               
        }

        public ActionResult RentList()
        {
            using (RentalCarEntities db = new RentalCarEntities())
            {
                return View(db.Reservations.ToList());
            }

        }

      

        // GET: Rent/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Rent/Create
        [HttpPost]
        public ActionResult Create(Reservation reservation)
        {
            try
            {
                using(RentalCarEntities db = new RentalCarEntities())
                {
                    db.Reservations.Add(reservation);
                    db.SaveChanges();
                }

                return Redirect("~/Home/Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Rent/Edit/5
        public ActionResult Edit(int id)
        {
            using (RentalCarEntities db = new RentalCarEntities())
            {
                return View(db.Reservations.Where(x => x.CarID == id).FirstOrDefault());
            }
        }

        // POST: Rent/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Reservation reservation )
        {
            try
            {
                using (RentalCarEntities db = new RentalCarEntities())
                {
                    db.Entry(reservation).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return Redirect("~/Home/Index");
            }
            catch
            {
                return View();
            }
        }

      
    }
}
