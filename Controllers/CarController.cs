﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication9.Models;

namespace WebApplication9.Controllers
{
    public class CarController : Controller
    {
        // GET: Car
        public ActionResult Index()
        {
            using(RentalCarEntities db = new RentalCarEntities())
            {
                var cars = db.Cars.SqlQuery(" select Cars.Plate,Cars.CarID,Cars.Manufacturer,Cars.Model,Cars.PricePerDay from Cars inner join Reservations on Reservations.CarID = Cars.CarID where Reservations.ReservStatsID <> 1");
                var cars2= db.Cars.SqlQuery("select Cars.Plate,Cars.CarID,Cars.Manufacturer,Cars.Model,Cars.PricePerDay from Cars where  CarID NOT IN  (SELECT CarID  FROM Reservations)");
                List<Car> list=cars.ToList();
                List<Car> list2 = cars2.ToList();
                 list.AddRange(list2);
                return View(list);
            }
            
        }

      
    }
}
